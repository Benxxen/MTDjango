from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # url(r'^Fish/', include('FishingReporter.urls')),
                       # (r'^$', RedirectView.as_view(url='/Guestbook/list/')),
                       # Examples:
                       # url(r'^$', 'MultiTenancy.views.home', name='home'),
                       # url(r'^MultiTenancy/', include('MultiTenancy.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       # url(r'^admin/', include(admin.site.urls)),
                       url(r'^mtadmin/', include('mtDjango.mtAdmin.urls')),
                       url(r'^university/', include('university_test_app.urls')),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
