from django.conf.urls import patterns, url
from django.conf.urls.static import static
from django.conf import settings

from university_test_app import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='u_index'),
                       url(r'^login/$', views.login_university, name='u_login'),
                       url(r'logout/$', views.logout_university, name='u_logout'),
                       url(r'give_marks/$', views.give_marks, name='give_marks'),
                       url(r'show_marks/$', views.show_marks, name='show_marks'),

                       ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
