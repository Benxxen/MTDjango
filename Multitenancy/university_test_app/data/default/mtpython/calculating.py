__author__ = 'olivermoege'

from mtDjango.fileLoader import tenantHolder


def calc_marks():
    # tenant_id = get_current_tenant_id()
    # w = PythonFileLoader("calculating.py", str(tenant_id))
    # w.load_tenant_module()

    method = tenantHolder.get_tenant_method(calc_marks.__name__)

    if method:
        try:
            return method()
        except:
            return 'error on loading method'
    return 'Do default action'

    # if method:
    #     string = method()
    # else:
    #     string = 'Do default'
    # return string
