from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand
from django.db import IntegrityError


def create_group(groupname):

    try:
        group = Group.objects.create(name=groupname)
    except IntegrityError:
        self.stdout.write('\n'+ groupname + ' permissions already created.\n')

class Command(BaseCommand):
    help = 'creates the groups of the unviversity test app'
    args = 'no arguments here'

    def handle(self, *args, **options):
        create_group('professors')
        create_group('students')



