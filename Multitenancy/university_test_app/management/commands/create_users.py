__author__ = 'olivermoege'
from django.contrib.auth.models import Group
from django.core.management import BaseCommand

from mtDjango.models import User


def create_user(username, email, password, groupname):
    user = User.objects.create_user(username, email, password)
    user.groups.add(Group.objects.get(name=groupname))


def del_user(username):
    pass


class Command(BaseCommand):
    help = 'creates the test users of the unviversity test app'
    args = 'no arguments here'

    def handle(self, *args, **options):
        create_user('Prof 007', 'prof@uni.de', 'pass', 'professors')
        create_user('Stud 001', 'student@uni.de', 'pass', 'students')

        print('Users created')
