__author__ = 'olivermoege'

from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import user_passes_test

from university_test_app.forms import LoginForm
from university_test_app.data.default.mtpython import calculating


def is_student(user):
    return user.is_authenticated() and user.groups.filter(name='students').exists()


def is_professor(user):
    return user.is_authenticated() and user.groups.filter(name='professors').exists()


def index(request):
    template = loader.select_template(['university_index.html'])
    user = request.user

    context = RequestContext(request, {
        'user_group': user.groups.first()
    })

    return HttpResponse(template.render(context))


@csrf_exempt
def login_university(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))
    login_failed = False
    if request.method == 'POST':
        form = LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('u_index'))
            else:
                print('useraccount is inactive')
        else:
            print('Invalid Login')

    else:
        form = LoginForm()

    context = RequestContext(request, {
        'form': form,
        'login_failed': login_failed
    })

    template = loader.select_template(['university_login.html'])

    return HttpResponse(template.render(context))


def logout_university(request):
    if request.user.is_authenticated():
        logout(request)

    return HttpResponseRedirect(reverse('u_index'))


@user_passes_test(is_student, login_url='/university/login/')
def show_marks(request):
    context = RequestContext(request, {
    })

    template = loader.select_template(['university_show_marks.html'])

    return HttpResponse(template.render(context))


@user_passes_test(is_professor, login_url='/university/login/')
def give_marks(request):
    context = RequestContext(request, {
        'dynamicText': calculating.calc_marks()
    })

    template = loader.select_template(['university_give_marks.html'])

    return HttpResponse(template.render(context))
