import threading

from django.utils.translation import ugettext as _
from django.http import HttpResponseNotFound
from django import http
from mtDjango.models import Tenant_Domain

# implementation of a middleware
# analyse the request a set the current tenant

class MTMiddleware(object):
    # analysing the request
    def process_request(self, request):
        url = http.HttpRequest.get_host(request)
        words = url.split(":")
        indomain = words.__getitem__(0)

        domain = Tenant_Domain.objects.get(name=indomain)
        if domain.tenant.active:
            setattr(threading.currentThread(), 'tenant', domain.tenant.id)
        else:
            output = "<h1>{}</h1>".format(_('Domain is currently inactive'))
            return HttpResponseNotFound(output)
