from django.conf.urls import patterns, url
from django.conf.urls.static import static
from django.conf import settings

from mtDjango.mtAdmin.view import login, content, administration

urlpatterns = patterns('',
                       url(r'^$', login.index, name='index'),
                       url(r'^login/$', login.login_user, name='login'),
                       url(r'^logout/$', login.logout_user, name='logout'),
                       url(r'^profile/(?P<user_id>.*)/$', administration.change_user_profile, name='profile'),

                       # url(r'^showTemplate/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.show_template,
                       #     name='showTemplate'),

                       url(r'^refreshInstance/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.refresh_instance,
                           name='refreshInstance'),

                       url(r'^getFileList/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.get_file_list,
                           name='getFileList'),
                       url(r'^createDir/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.create_dir,
                           name='createDir'),
                       url(r'^renameDir/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.rename_dir,
                           name='renameDir'),
                       url(r'^deleteDir/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.delete_dir,
                           name='deleteDir'),
                       url(r'^deleteFile/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.delete_file,
                           name='deleteFile'),
                       url(r'^uploadFile/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.upload_file,
                           name='uploadFile'),
                       url(r'^renameFile/(?:(?P<tenant_id>[0-9]*)?)/(?P<path>.*)/$', content.rename_file,
                           name='renameFile'),

                       url(r'^UserList/$', administration.get_user_list,
                           name='getUserList'),
                       # url(r'^instanceAdmin/(?P<tenant_id>[0-9]*)/$', administration.get_instance_admin_list_by_tenant,
                       #     name='getInstanceAdminListByTenant'),
                       url(r'^createUser/$', administration.create_user, name='createUser'),
                       url(r'^changeUserPassword/(?P<user_id>.*)/$',
                           administration.change_user_password,
                           name='changeUserPassword'),
                       url(r'^deleteUser/(?P<user_id>.*)/$', administration.delete_user,
                           name='deleteUser'),
                       url(r'^changeUserPermission/(?P<user_id>.*)/$', administration.change_user_permission,
                           name='changeUserPermission'),

                       url(r'^modDBConn/(?P<tenant_id>[0-9]+)/$', administration.modify_db_connection,
                           name='modDBConn'),
                       url(r'^tenants/$', administration.get_tenant_list, name='getTenants'),
                       url(r'^createTenant/$', administration.create_tenant, name='createTenant'),
                       url(r'^changeTenant/(?P<tenant_id>[0-9]*)/$', administration.change_tenant, name='changeTenant'),
                       url(r'^deleteTenant/(?P<tenant_id>[0-9]+)/$', administration.delete_tenant, name='deleteTenant'),
                       url(r'^getDomains/(?P<tenant_id>[0-9]+)/$', administration.get_domains, name='getDomains'),
                       url(r'^deleteDomain/(?P<domain>.*)/$', administration.delete_domain, name='deleteDomain'),
                       url(r'^createDomain/(?P<tenant_id>[0-9]+)/$', administration.create_domain, name='createDomain'),

                       ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
