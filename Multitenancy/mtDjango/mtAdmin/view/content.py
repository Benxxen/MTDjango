import os
import shutil
import logging
import re

from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse
from django.template import RequestContext
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.utils.translation import ugettext as _

from mtDjango.fileLoader.pythonfileloader import PythonFileLoader
from mtDjango.general import get_current_tenant_id, get_current_tenant
from mtDjango.models import Tenant
from mtDjango.mtAdmin.forms import CreateDirForm, DocumentForm


# get logger instance
from mtDjango.mtAdmin.view.login import is_instance_admin

log = logging.getLogger(__name__)

# ##################################################################################
# Template Processing

# show the template in the online editor
@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def show_template(request, path):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    data_dir = os.path.dirname(settings.TENANT_DIR)
    tenant_id = get_current_tenant_id()

    work_file = '{0}{1}{2}{1}templates{1}{3}'.format(data_dir, os.sep, tenant_id, path)

    file = open(work_file, "r")

    # text = file.readlines()
    text = file.read()

    file.close()

    context = RequestContext(request, {
        'entry': text,
        'path': path,
    })

    template = loader.select_template(['contentAdministration' + os.sep + 'mtadmin_template_show.html'])

    return HttpResponse(template.render(context))


# save the editing template in the templateDIR
@csrf_exempt
@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def save_template(request, path):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    if request.method == 'POST':
        text = request.POST['template']

        data_dir = os.path.dirname(settings.TENANT_DIR)
        tenant_id = get_current_tenant_id()

        work_file = '{0}{1}{2}{1}templates{1}{3}'.format(data_dir, os.sep, tenant_id, path)

        file = open(work_file, "w")

        print(text)

        file.write(text)

        file.close()

        return HttpResponseRedirect(reverse('getTemplateList', kwargs={'path': '/'}))




# method to rename a template
@csrf_exempt
@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def rename_template(request, path):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    if request.method == 'POST':
        directory = request.POST['name']
        data_dir = os.path.dirname(settings.TENANT_DIR)
        tenant_id = get_current_tenant_id()

        work_directory = '{0}{1}{2}{1}templates{1}{3}'.format(data_dir, os.sep, tenant_id, path)
        new_dir = '{0}{1}{2}{1}templates{1}{3}{4}'.format(data_dir, os.sep, tenant_id, get_return_path(path), directory)
        print(work_directory)
        print(new_dir)
        if os.path.exists(work_directory):
            os.rename(work_directory, new_dir)

        new_path = get_return_path(path)
        return HttpResponseRedirect(reverse('getTemplateList', kwargs={'path': new_path}))
    else:
        form = CreateDirForm()
        context = RequestContext(request, {
            'param': path,
            'form': form,
            'title': _('Rename file'),
            'sendUrl': 'renameTemplate'
        })

        template = loader.select_template(['mtadmin_form_with_param.html'])
        return HttpResponse(template.render(context))


# method to delete a template
@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def delete_template(request, path):
    data_dir = os.path.dirname(settings.TENANT_DIR)
    tenant_id = get_current_tenant_id()

    work_directory = '{0}{1}{2}{1}templates{1}{3}'.format(data_dir, os.sep, tenant_id, path)

    os.remove(work_directory)
    path = get_return_path(path)
    print(path)
    return HttpResponseRedirect(reverse('getTemplateList', kwargs={'path': path}))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def get_file_list(request, path, tenant_id=None):
    if not tenant_id:
        actual_tenant = get_current_tenant()
    else:
        actual_tenant = Tenant.objects.get(id=tenant_id)

    data_dir = os.path.dirname(settings.TENANT_DIR)
    work_directory = '{0}{1}{2}{3}'.format(data_dir, os.sep, actual_tenant.id, path)
    out_dir = []
    out_file = []

    # prepare path names for breadcrumbs
    element_path_names = re.split(os.sep, path)
    while '' in element_path_names:
        element_path_names.remove('')

    # prepare element path
    breadcrumbs = []
    element_path = ''
    if element_path_names is not None and len(element_path_names) != 0:
        for element in element_path_names:
            element_path += '/' + element
            breadcrumbs.append(Breadcrumb(element_path, element, False))
        breadcrumbs[-1].active = True

    for file in os.listdir(work_directory):
        if os.path.isdir(work_directory + os.sep + file):
            out_dir.append(file)
        else:
            out_file.append(file)

    user_tenants = request.user.tenant_set.all()
    if len(user_tenants) < 2:
        user_tenants = None

    out_dir.sort()
    out_file.sort()

    is_root = False
    if len(element_path_names) == 0:
        is_root = True

    context = RequestContext(request, {
        'isRoot': is_root,
        'dir': out_dir,
        'file': out_file,
        'path': path,
        'breadcrumbs': breadcrumbs,
        'title': _('Static files'),
        'userTenants': user_tenants,
        'actualTenant': actual_tenant,
    })

    template = loader.select_template(['contentAdministration' + os.sep + 'mtadmin_file_list.html'])

    return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def create_dir(request, path, tenant_id=None):
    if request.method == 'POST':
        directory = request.POST['name']
        data_dir = os.path.dirname(settings.TENANT_DIR)
        if not tenant_id:
            tenant_id = get_current_tenant_id()

        work_directory = '{0}{1}{2}{3}{1}{4}'.format(data_dir, os.sep, tenant_id, path, directory)
        if not os.path.exists(work_directory):
            os.makedirs(work_directory)

        new_path = path + '/' + directory
        return HttpResponseRedirect(reverse('getFileList', kwargs={'path': new_path}))
    else:
        form = CreateDirForm()
        context = RequestContext(request, {
            'param': path,
            'form': form,
            'title': _('Create folder'),
            'sendUrl': 'createDir'
        })

        template = loader.select_template(['mtadmin_form_with_param.html'])
        return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def rename_dir(request, path, tenant_id=None):
    if request.method == 'POST':
        directory = request.POST['name']
        data_dir = os.path.dirname(settings.TENANT_DIR)
        if not tenant_id:
            tenant_id = get_current_tenant_id()

        work_directory = '{0}{1}{2}{1}{3}'.format(data_dir, os.sep, tenant_id, path)
        new_dir = '{0}{1}{2}{1}{3}{4}'.format(data_dir, os.sep, tenant_id, get_return_path(path), directory)
        if os.path.exists(work_directory):
            os.rename(work_directory, new_dir)

        new_path = get_return_path(path)
        return HttpResponseRedirect(reverse('getFileList', kwargs={'path': new_path}))
    else:
        form = CreateDirForm()
        context = RequestContext(request, {
            'param': path,
            'form': form,
            'title': _('Rename folder'),
            'sendUrl': 'renameDir'
        })

        template = loader.select_template(['mtadmin_form_with_param.html'])
        return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def delete_dir(request, path, tenant_id=None):
    data_dir = os.path.dirname(settings.TENANT_DIR)
    if not tenant_id:
        tenant_id = get_current_tenant_id()

    work_directory = '{0}{1}{2}{3}'.format(data_dir, os.sep, tenant_id, path)

    shutil.rmtree(work_directory)
    path = get_return_path(path)
    return HttpResponseRedirect(reverse('getFileList', kwargs={'path': path}))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def delete_file(request, path, tenant_id=None):
    data_dir = os.path.dirname(settings.TENANT_DIR)
    if not tenant_id:
        tenant_id = get_current_tenant_id()

    file_path = '{0}{1}{2}{3}'.format(data_dir, os.sep, tenant_id, path)

    os.remove(file_path)
    path = get_return_path(path)
    return HttpResponseRedirect(reverse('getFileList', kwargs={'path': path}))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def upload_file(request, path, tenant_id=None):
    if request.method == 'POST':
        filename = request.FILES['file'].name
        raw_data = request.FILES['file']

        data_dir = os.path.dirname(settings.TENANT_DIR)
        if not tenant_id:
            tenant_id = get_current_tenant_id()

        work_directory = '{0}{1}{2}{1}{3}{4}'.format(data_dir, os.sep, tenant_id, path, filename)
        if not os.path.isfile(work_directory):
            file = open(work_directory, "wb+")
            for chunk in raw_data.chunks():
                file.write(chunk)

        return HttpResponseRedirect(reverse('getFileList', kwargs={'path': path}))
    else:
        form = DocumentForm()
        context = RequestContext(request, {
            'path': path,
            'form': form,
            'title': _('Static file upload'),
            'uploadUrl': 'uploadFile'
        })

        template = loader.select_template(['contentAdministration' + os.sep + 'mtadmin_upload.html'])
        return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def rename_file(request, path, tenant_id=None):
    if request.method == 'POST':
        directory = request.POST['name']
        data_dir = os.path.dirname(settings.TENANT_DIR)
        if not tenant_id:
            tenant_id = get_current_tenant_id()

        work_directory = '{0}{1}{2}{1}{3}'.format(data_dir, os.sep, tenant_id, path)
        new_dir = '{0}{1}{2}{1}{3}{4}'.format(data_dir, os.sep, tenant_id, get_return_path(path), directory)
        if os.path.exists(work_directory):
            os.rename(work_directory, new_dir)

        new_path = get_return_path(path)
        return HttpResponseRedirect(reverse('getFileList', kwargs={'path': new_path}))
    else:
        form = CreateDirForm()
        context = RequestContext(request, {
            'param': path,
            'form': form,
            'title': _('Rename file'),
            'sendUrl': 'renameFile'
        })

        template = loader.select_template(['mtadmin_form_with_param.html'])
        return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def refresh_instance(request, path, tenant_id=None):
    if not tenant_id:
        tenant_id = get_current_tenant_id()

    if path:
        path = path[1:]
        name = path[path.rfind(os.sep) + 1:]
        w = PythonFileLoader(name, str(tenant_id))
        w.load_tenant_module()

    path = get_return_path(path)
    return HttpResponseRedirect(reverse('getFileList', kwargs={'path': path}))


# method to generate a return path
def get_return_path(path):
    if len(path) != 1:
        path = path[:-1]
        last_index = path.rfind(os.sep)
        path = path[:last_index + 1]
    return path


class Breadcrumb:
    def __init__(self, path, name, active):
        self.path = path
        self.name = name
        self.active = active
