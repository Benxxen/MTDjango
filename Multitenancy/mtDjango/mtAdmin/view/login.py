import logging

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.template import RequestContext
from django.template import loader
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from mtDjango.general import get_current_tenant_id
from mtDjango.models import GROUP_SYSTEM_ADMINISTRATOR, GROUP_INSTANCE_ADMINISTRATOR
from mtDjango.mtAdmin.forms import LoginForm

# get logger instance
log = logging.getLogger(__name__)


@login_required(login_url="/mtadmin/login/")
def index(request):
    return HttpResponseRedirect(reverse('getFileList', args='/'))


def is_in_system_admin_group(user):
    return user.groups.filter(name=GROUP_SYSTEM_ADMINISTRATOR).exists()


# ##################################################################################
# Authentication and login methods
def is_system_admin(user):
    return user.is_authenticated() and user.groups.filter(name=GROUP_SYSTEM_ADMINISTRATOR).exists()


def is_instance_admin(user):
    return user.is_authenticated() and user.groups.filter(name=GROUP_INSTANCE_ADMINISTRATOR).exists()


def login_user(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))
    login_failed = True
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            user_for_check = None
            if User.objects.filter(username=username).exists():
                user_for_check = User.objects.get(username=username)
            elif User.objects.filter(
                    username='{0}_{1}'.format(get_current_tenant_id(), username)).exists():
                username = '{0}_{1}'.format(get_current_tenant_id(), username)
                user_for_check = User.objects.get(username=username)

            if user_for_check and (user_for_check.tenant_set.filter(
                    id=get_current_tenant_id()).exists() or is_in_system_admin_group(user_for_check)):

                user = authenticate(username=username, password=form.cleaned_data['password'])
                if user is not None and user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('index'))
    else:
        login_failed = False
        form = LoginForm()

    context = RequestContext(request, {
        'form': form,
        'login_failed': login_failed
    })

    template = loader.select_template(['mtadmin_login.html'])
    return HttpResponse(template.render(context))


def logout_user(request):
    if request.user.is_authenticated():
        logout(request)

    return HttpResponseRedirect(reverse('login'))
