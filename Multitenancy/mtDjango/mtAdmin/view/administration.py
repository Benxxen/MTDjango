import copy
import os
import logging
import re

from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.template import RequestContext
from django.template import loader
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.utils.translation import ugettext as _

from mtDjango.models import Tenant_Domain, Tenant_DBConn, GROUP_SYSTEM_ADMINISTRATOR, GROUP_INSTANCE_ADMINISTRATOR
from mtDjango.models import Tenant
from mtDjango.mtAdmin.forms import TenantDomainForm, \
    ChangePasswordForm, TenantDBConnForm, CreateTenantForm, \
    ChangeTenantForm, CreateUserForm, ChangePermissionForm, ChangeInstanceAdminProfileForm, ChangeSystemAdminProfileForm



# get logger instance
from mtDjango.mtAdmin.view.login import is_system_admin, is_in_system_admin_group, is_instance_admin

log = logging.getLogger(__name__)

# ##################################################################################
# System admin methods

@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def get_tenant_list(request):
    tenants = Tenant.objects.all()

    context = RequestContext(request, {
        'tenant_list': tenants,
    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_tenant_list.html'])

    return HttpResponse(template.render(context))


@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def change_tenant(request, tenant_id):
    if not tenant_id:
        return HttpResponseRedirect(reverse('getTenants'))

    try:
        invalid = False
        tenant = Tenant.objects.get(id=tenant_id)
        if request.method == 'POST':
            tenant = Tenant.objects.get(id=tenant_id)
            form = ChangeTenantForm(request.POST)
            if form.is_valid():
                tenant.name = form.cleaned_data['name']
                tenant.email = form.cleaned_data['email']
                tenant.active = form.cleaned_data['active']
                tenant.save()
                return HttpResponseRedirect(reverse('getTenants'))
            else:
                invalid = True
        else:
            initial_values = {'name': tenant.name, 'email': tenant.email, 'active': tenant.active}
            form = ChangeTenantForm(initial=initial_values)

        context = RequestContext(request, {
            'param': tenant_id,
            'form': form,
            'invalid': invalid,
            'title': _('Edit tenant'),
            'sendUrl': 'changeTenant'
        })
        template = loader.select_template(['mtadmin_form_with_param.html'])
        return HttpResponse(template.render(context))

    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('getTenants'))


@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def create_tenant(request):
    invalid = False
    if request.method == 'POST':
        form = CreateTenantForm(request.POST)
        if form.is_valid():
            tenant = Tenant.objects.create(name=form.cleaned_data['name'], email=form.cleaned_data['email'],
                                           active=form.cleaned_data['active'])
            tenant.save()
            Tenant_Domain.objects.create(name=form.cleaned_data['domain'], tenant_id=tenant.id)

            data_dir = str(os.path.dirname(settings.TENANT_DIR)) + os.sep + str(tenant.id) + os.sep

            temp_dir = data_dir + 'templates' + os.sep
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir)

            temp_dir = data_dir + 'static' + os.sep
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir)

            temp_dir = data_dir + 'media' + os.sep
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir)

            temp_dir = data_dir + 'mtpython' + os.sep
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir)

            return HttpResponseRedirect(reverse('getTenants'))
        else:
            invalid = True
    else:
        form = CreateTenantForm()

    context = RequestContext(request, {
        'invalid': invalid,
        'form': form,
        'title': _('Create tenant'),
        'sendUrl': 'createTenant'
    })

    template = loader.select_template(['mtadmin_form.html'])
    return HttpResponse(template.render(context))


# method to delete a tenant
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def delete_tenant(request, tenant_id):
    try:
        tenant = Tenant.objects.get(id=tenant_id)
        if tenant is not None:
            tenant.delete()
    except ObjectDoesNotExist:
        log.warn('Tenant with id {} does not exist. So it was not deleted'.format(tenant_id))
    return HttpResponseRedirect(reverse('getTenants'))


# method to list all domains from a tenant
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def get_domains(request, tenant_id):
    domain_list = Tenant_Domain.objects.filter(tenant=tenant_id)

    context = RequestContext(request, {
        'domain_list': domain_list,
        'tenant_id': tenant_id,
    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_tenant_domain_list.html'])

    return HttpResponse(template.render(context))


# method to delete a domain
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def delete_domain(request, domain):
    try:
        domain_object = Tenant_Domain.objects.get(name=domain)
        tenant_id = domain_object.tenant.id
        domain_object.delete()
    except ObjectDoesNotExist:
        log.warn('Domain was not deleted (no domain: {})'.format(domain))
        return HttpResponseRedirect(reverse('getTenants'))
    return HttpResponseRedirect(reverse('getDomains', args=(tenant_id,)))


# method to create a domain
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def create_domain(request, tenant_id):
    invalid = False
    if request.method == 'POST':
        form = TenantDomainForm(request.POST)
        if form.is_valid():
            try:
                tenant = Tenant.objects.get(id=tenant_id)
                domain = Tenant_Domain(name=form.cleaned_data['name'], tenant=tenant)
                domain.save()
            except ObjectDoesNotExist:
                return HttpResponseRedirect(reverse('getTenants'))

            return HttpResponseRedirect(reverse('getDomains', args=(tenant_id,)))
        else:
            invalid = True
    else:
        form = TenantDomainForm()

    context = RequestContext(request, {
        'invalid': invalid,
        'form': form,
        'tenant_id': tenant_id,
    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_tenant_domain_create.html'])
    return HttpResponse(template.render(context))


@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def get_system_admin_list(request):
    group = Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR)
    auth_users = group.user_set.all()
    context = RequestContext(request, {
        'adminList': auth_users,
        'title': _('System Administrators '),
        'urlCreateNew': 'createSystemAdmin',
        'urlChangePassword': 'changeSystemAdminPassword',
        'urlDelete': 'deleteSystemAdmin',
        # 'urlAddPermission': 'addPermissionSystemAdmin'

    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_user_list.html'])

    return HttpResponse(template.render(context))


# method to change the password
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def change_system_admin_password(request, user_id):
    invalid = False
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            try:
                system_admin = User.objects.get(id=user_id)
                system_admin.set_password(form.cleaned_data['password'])
                system_admin.save()
                return HttpResponseRedirect(reverse('getSystemAdminList'))
            except ObjectDoesNotExist:
                return HttpResponseRedirect(reverse('getSystemAdminList'))
        else:
            invalid = True
    else:
        form = ChangePasswordForm()

    context = RequestContext(request, {
        'form': form,
        'pram': user_id,
        'invalid': invalid,
        'title': _('Change password'),
        'sendUrl': 'changeSystemAdminPassword'
    })
    template = loader.select_template(['mtadmin_form_with_param.html'])
    return HttpResponse(template.render(context))


# method to delete a user
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def delete_system_admin(user_id):
    try:
        user = User.objects.get(id=user_id)
        user.delete()
    except ObjectDoesNotExist:
        log.warn('System admin with id {} does not exist. So it was not deleted'.format(user_id))
    return HttpResponseRedirect(reverse('getSystemAdminList'))


# method to config the database

@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def modify_db_connection(request, tenant_id):
    invalid = False
    if request.method == 'POST':
        form = TenantDBConnForm(request.POST)
        if form.is_valid():
            try:
                tenant = Tenant.objects.get(id=tenant_id)
                db_connection = Tenant_DBConn()
                db_connection.tenant = tenant
                db_connection.hostname = form.cleaned_data['hostname']
                db_connection.port = form.cleaned_data['port']
                db_connection.username = form.cleaned_data['username']
                db_connection.dbName = form.cleaned_data['dbName']
                db_connection.password = form.cleaned_data['password']
                db_connection.engine = form.cleaned_data['engine']
                db_connection.save()
                return HttpResponseRedirect(reverse('getTenants'))
            except ObjectDoesNotExist:
                log.warn('DBConnection was not modified (no tenant with id {})'.format(tenant_id))
                return HttpResponseRedirect(reverse('getTenants'))
        else:
            invalid = True

    else:

        try:
            db_connection = Tenant_DBConn.objects.get(tenant_id=tenant_id)
        except ObjectDoesNotExist:
            db_connection = None
        if db_connection:
            form = TenantDBConnForm(instance=db_connection)
        else:
            form = TenantDBConnForm()

    context = RequestContext(request, {
        'invalid': invalid,
        'form': form,
        'param': tenant_id,
        'sendUrl': 'modDBConn',
        'title': _('Edit database-connection')
    })

    template = loader.select_template(['mtadmin_form_with_param.html'])

    return HttpResponse(template.render(context))


# method to list all user
# @user_passes_test(is_system_admin, login_url='/mtadmin/login/')
# def get_instance_admin_list(request):
#     group = Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR)
#     instance_admin_list = group.user_set.all()
#
#     context = RequestContext(request, {
#         'adminList': instance_admin_list,
#         'title': _('Instance Administrators'),
#         'urlCreateNew': 'createInstanceAdmin',
#         'urlChangePassword': 'changeInstanceAdminPassword',
#         'urlDelete': 'deleteInstanceAdmin'
#     })
#
#     template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_user_list.html'])
#
#     return HttpResponse(template.render(context))

@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def get_user_list(request):
    user_list = copy.copy(User.objects.all())
    for user in user_list:
        if not is_in_system_admin_group(user):
            user.username = re.split('_', user.username)[1]

    context = RequestContext(request, {
        'adminList': user_list,
        'title': _('User'),
    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_user_list.html'])

    return HttpResponse(template.render(context))


# method to list all user
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def get_instance_admin_list_by_tenant(request, tenant_id):
    if tenant_id:
        try:
            tenant = Tenant.objects.get(id=tenant_id)
            instance_admin_list = tenant.user.all()
        except ObjectDoesNotExist:
            tenant_id = 0
            tenants = Tenant.objects.all()
            instance_admin_list = tenants[0].user.all()

    else:
        tenants = Tenant.objects.all()
        instance_admin_list = tenants[0].user.all()

    context = RequestContext(request, {
        'adminList': instance_admin_list,
        'title': _('Instance Administrators'),
        'urlCreateNew': 'createInstanceAdmin',
        'urlChangePassword': 'changeInstanceAdminPassword',
        'urlDelete': 'deleteInstanceAdmin',
        'tenant_id': tenant_id
    })

    template = loader.select_template(['systemAdministration' + os.sep + 'mtadmin_user_list.html'])

    return HttpResponse(template.render(context))


@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def create_user(request):
    invalid = False
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():

            if form.cleaned_data['is_sysadmin']:
                user = User.objects.create_superuser(form.cleaned_data['username'],
                                                     form.cleaned_data['email'],
                                                     form.cleaned_data['password'])
                user.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
            else:
                tenant = form.cleaned_data['tenant']
                user = User.objects.create_user('{0}_{1}'.format(tenant.id, form.cleaned_data['username']),
                                                form.cleaned_data['email'],
                                                form.cleaned_data['password'])

                user.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
                Tenant.objects.get(id=tenant.id).user.add(user)
            return HttpResponseRedirect(reverse('getUserList'))
        else:
            invalid = True
    else:
        form = CreateUserForm()

    context = RequestContext(request, {
        'form': form,
        'invalid': invalid,
        'title': _('Create user'),
        'sendUrl': 'createUser'
    })

    template = loader.select_template(['mtadmin_form.html'])

    return HttpResponse(template.render(context))


# method to change the password
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def change_user_password(request, user_id):
    invalid = False
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            try:
                instance_admin = User.objects.get(id=user_id)
                instance_admin.set_password(form.cleaned_data['password'])
                instance_admin.save()
                return HttpResponseRedirect(reverse('getUserList'))
            except ObjectDoesNotExist:
                return HttpResponseRedirect(reverse('getUserList'))
        else:
            invalid = True
    else:
        form = ChangePasswordForm()

    context = RequestContext(request, {
        'form': form,
        'param': user_id,
        'invalid': invalid,
        'title': _('Change password'),
        'sendUrl': 'changeUserPassword'
    })
    template = loader.select_template(['mtadmin_form_with_param.html'])
    return HttpResponse(template.render(context))


# method to delete a user
@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def delete_user(request, user_id):
    try:
        user = User.objects.get(id=user_id)
        user.delete()
    except ObjectDoesNotExist:
        log.warn('User with id {} does not exist. So it was not deleted'.format(user_id))

    return HttpResponseRedirect(reverse('getUserList'))


@user_passes_test(is_system_admin, login_url='/mtadmin/login/')
def change_user_permission(request, user_id):
    invalid = False
    if request.method == 'POST':
        form = ChangePermissionForm(request.POST)
        if form.is_valid():
            user = User.objects.get(id=form.cleaned_data['hidden_user_id'])
            was_sysadmin = form.cleaned_data['hidden_was_sysadmin']
            is_sysadmin = form.cleaned_data['is_sysadmin']
            tenant = form.cleaned_data['tenant']
            if was_sysadmin is not is_sysadmin:
                if not was_sysadmin and is_sysadmin:
                    splitted_name = re.split('_', user.username)
                    user.username = splitted_name[1]
                    user.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
                    if tenant:
                        Tenant.objects.get(id=tenant.id).user.remove(user)
                    else:
                        Tenant.objects.get(id=splitted_name[0]).user.remove(user)
                elif was_sysadmin and not is_sysadmin:
                    user.username = '{0}_{1}'.format(tenant.id, user.username)
                    user.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
                    user.groups.remove(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
                    Tenant.objects.get(id=tenant.id).user.add(user)
            user.save()
            return HttpResponseRedirect(reverse('getUserList'))
        else:
            invalid = True
    if not invalid:
        try:
            user = User.objects.get(id=user_id)
            user_is_system_admin = is_in_system_admin_group(user)
            if not user_is_system_admin:
                initial_values = {'username': re.split('_', user.username)[1], 'is_sysadmin': user_is_system_admin,
                                  'tenant_id': re.split('_', user.username)[0], 'hidden_user_id': user.id,
                                  'hidden_was_sysadmin': user_is_system_admin}
            else:
                initial_values = {'username': user.username, 'is_sysadmin': user_is_system_admin,
                                  'hidden_user_id': user.id,
                                  'hidden_was_sysadmin': user_is_system_admin}
            form = ChangePermissionForm(initial=initial_values)
        except ObjectDoesNotExist:
            log.warn("User with id {} does not exist. So the permission can't be changed.".format(user_id))
            return HttpResponseRedirect(reverse('getUserList'))

    context = RequestContext(request, {
        'form': form,
        'param': user_id,
        'invalid': invalid,
        'title': _('Change permission'),
        'sendUrl': 'changeUserPermission'
    })
    template = loader.select_template(['mtadmin_form_with_param_cancel.html'])
    return HttpResponse(template.render(context))


@user_passes_test(is_instance_admin, login_url='/mtadmin/login/')
def change_user_profile(request, user_id):
    invalid = False
    user = User.objects.get(id=user_id)
    is_sysadmin = is_in_system_admin_group(user)
    if request.method == 'POST':
        if is_sysadmin:
            form = ChangeSystemAdminProfileForm(request.POST)
        else:
            form = ChangeInstanceAdminProfileForm(request.POST)

        if form.is_valid():
            try:
                old_username = user.username
                new_username = form.cleaned_data['username']
                if new_username and new_username is not old_username:
                    user.username = new_username
                new_email = form.cleaned_data['email']
                old_email = user.email
                if new_email and new_email is not old_email:
                    user.email = new_email

                user.set_password(form.cleaned_data['password'])
                user.save()

                return HttpResponseRedirect(reverse('index'))
            except ObjectDoesNotExist:
                return HttpResponseRedirect(reverse('index'))
        else:
            invalid = True
    else:
        if is_sysadmin:
            form = ChangeSystemAdminProfileForm(
                initial={'username': user.username, 'email': user.email, 'hidden_user_id': user.id})

        else:
            form = ChangeInstanceAdminProfileForm(
                initial={'username': re.split('_', user.username)[1], 'email': user.email,
                         'hidden_tenant_id': re.split('_', user.username)[0], 'hidden_user_id': user.id})

    context = RequestContext(request, {
        'form': form,
        'param': user_id,
        'invalid': invalid,
        'title': _('Change profile'),
        'sendUrl': 'profile'
    })
    template = loader.select_template(['mtadmin_form_with_param.html'])
    return HttpResponse(template.render(context))
