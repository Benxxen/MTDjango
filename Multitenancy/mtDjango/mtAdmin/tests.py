import os
from sqlite3 import collections

from django.contrib.auth.models import User, Group

from django.core.exceptions import ObjectDoesNotExist

from django.core.urlresolvers import reverse

from django.test import TestCase, Client

from django.test.utils import setup_test_environment

from Multitenancy import settings
from mtDjango.management.commands.initpermissions import init_permissions
from mtDjango.models import Tenant, Tenant_Domain, GROUP_SYSTEM_ADMINISTRATOR, GROUP_INSTANCE_ADMINISTRATOR


class LoginTests(TestCase):
    def setUp(self):
        self.client = Client()
        setup_test_environment()  # install a template renderer
        init_permissions(self)

        tenant = Tenant.objects.create(name='tenant1', email='tenant1@mt.de', active=True)
        tenant.save()
        self.tenant_id = tenant.id
        Tenant_Domain.objects.create(name='testserver', tenant=tenant)

    def test_login(self):
        sysadmin_name = 'sysadmin'
        instance_admin_name = '0_instanceAdmin'

        system_admin = User.objects.create_user(sysadmin_name, 'sa@mt.de', '123456789')
        system_admin.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
        system_admin.save()

        response = self.client.post(reverse('login'), {'username': sysadmin_name, 'password': 'wrong_password'})
        self.assertTrue(response.context['login_failed'], 'Login in password check failed')

        response = self.client.post(reverse('login'), {'username': sysadmin_name, 'password': '123456789'})
        self.assertRedirects(response, reverse('index'), 302, 302, None, "System admin login failed")
        response = self.client.post(reverse('logout'))
        self.assertRedirects(response, reverse('login'), 302, 200, None, "System admin logout failed")

        instance_admin = User.objects.create_user(instance_admin_name, 'ia@mt.de', '123456789')
        instance_admin.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
        response = self.client.post(reverse('login'), {'username': instance_admin_name, 'password': '123456789'})
        self.assertTrue(response.context['login_failed'], 'Tenant check in instance admin failed')

        instance_admin.tenant_set.add(Tenant.objects.get(id=self.tenant_id))
        response = self.client.post(reverse('login'), {'username': instance_admin_name, 'password': '123456789'})
        self.assertRedirects(response, reverse('index'), 302, 302, None, "Instance admin login failed")
        response = self.client.post(reverse('logout'))
        self.assertRedirects(response, reverse('login'), 302, 200, None, "Instance admin logout failed")


class TenantTests(TestCase):
    def setUp(self):
        self.client = Client()
        setup_test_environment()  # install a template renderer
        init_permissions(self)
        sysadmin_name = 'testuser'

        user = User.objects.create_user(sysadmin_name, 'test1@mt.de', '123456789')
        user.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
        user.save()

        tenant = Tenant.objects.create(name='tenant1', email='tenant1@mt.de', active=True)
        tenant.save()
        self.tenant_id = tenant.id
        Tenant_Domain.objects.create(name='testserver', tenant=tenant)
        self.client.login(username=sysadmin_name, password='123456789')

    def test_create_tenant(self):
        domain_name = 'testDomain'
        tenant_name = 'testTenant'

        self.client.post(reverse('createTenant'),
                         {'domain': domain_name, 'name': tenant_name, 'email': 'ttmail@mt.de', 'active': True})
        tenant = None
        try:
            tenant = Tenant.objects.get(name=tenant_name)
        except ObjectDoesNotExist:
            self.assertTrue(False, 'Tenant creation failed')

        data_dir = str(os.path.dirname(settings.TENANT_DIR)) + os.sep + str(tenant.id) + os.sep
        templates_dir = data_dir + 'templates' + os.sep
        static_dir = data_dir + 'static' + os.sep
        media_dir = data_dir + 'media' + os.sep
        python_dir = data_dir + 'mtpython' + os.sep
        if not os.path.exists(templates_dir) or not os.path.exists(static_dir) or not os.path.exists(
                media_dir) or not os.path.exists(python_dir):
            self.assertTrue(False, 'Tenant directories were not created')

        tenant_id = None
        try:
            tenant_id = Tenant_Domain.objects.get(name=domain_name).tenant_id
        except ObjectDoesNotExist:
            self.assertTrue(False, 'Tenant to domain association failed')

        self.assertEqual(tenant.id, tenant_id, 'Tenant to domain association failed')

        response = self.client.post(reverse('createTenant'),
                                    {'domain': 'testDomain2', 'name': 'testTenant2', 'email': 't',
                                     'active': True})
        self.assertTrue(response.context['invalid'], 'Tenant creation mail check is invalid')

    def test_delete_tenant(self):
        self.client.post(reverse('createTenant'),
                         {'domain': 'testDomain3', 'name': 'tenant_to_delete', 'email': 'ttmail4@mt.de',
                          'active': True})
        tenant = Tenant.objects.get(name='tenant_to_delete')
        self.client.post(reverse('deleteTenant', args=[tenant.id]))

        self.assertFalse(Tenant.objects.filter(name='tenant_to_delete').exists(), 'Tenant deletion failed')
        self.assertFalse(Tenant_Domain.objects.filter(name='testDomain3').exists(),
                         'Tenant deletion failed (domain was not deleted)')

        self.assertRedirects(self.client.post(reverse('deleteTenant', args=[42])), reverse('getTenants'))

    def test_change_tenant(self):
        self.client.post(reverse('createTenant'),
                         {'domain': 'testDomain4', 'name': 'tenant_to_change', 'email': 'ttmail5@mt.de',
                          'active': True})
        tenant_id = Tenant.objects.get(name='tenant_to_change').id
        self.client.post(reverse('changeTenant', args=[tenant_id]),
                         {'name': 'tenant_changed', 'email': 'ttmail5@mt.de', 'active': True})
        self.assertEqual(Tenant.objects.get(id=tenant_id).name, 'tenant_changed', 'Changing tenant name failed')
        self.client.post(reverse('changeTenant', args=[tenant_id]),
                         {'name': 'tenant_changed', 'email': 'changedmail@mt.de', 'active': True})
        self.assertEqual(Tenant.objects.get(id=tenant_id).email, 'changedmail@mt.de', 'Changing tenant email failed')
        self.client.post(reverse('changeTenant', args=[tenant_id]),
                         {'name': 'tenant_changed', 'email': 'changedmail@mt.de', 'active': False})
        self.assertFalse(Tenant.objects.get(id=tenant_id).active, 'Changing tenant activation state failed')

    def test_read_tenant_list(self):
        compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        response = self.client.post(reverse('getTenants'))
        self.assertTrue(compare(Tenant.objects.all(), response.context['tenant_list']),
                        'The responded list of tenants from server is incorrect')


class InstanceAdminTests(TestCase):
    def setUp(self):
        self.client = Client()
        setup_test_environment()  # install a template renderer
        init_permissions(self)
        sysadmin_name = 'testuser'

        user = User.objects.create_user(sysadmin_name, 'test1@mt.de', '123456789')
        user.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
        user.save()

        tenant = Tenant.objects.create(name='tenant1', email='tenant1@mt.de', active=True)
        tenant.save()
        self.tenant_id = tenant.id
        self.tenant = tenant
        Tenant_Domain.objects.create(name='testserver', tenant=tenant)
        self.client.login(username=sysadmin_name, password='123456789')

    def test_create_instance_admin(self):
        instance_admin_name = 'instanceAdmin1'
        instance_admin_name_prefix = '{0}_{1}'.format(self.tenant_id, instance_admin_name)
        instance_admin_email = 'insadmin1@mt.de'

        self.client.post(reverse('createUser'),
                         {'username': instance_admin_name, 'email': instance_admin_email,
                          'password': '1234567', 'password_retry': '1234567', 'tenant': self.tenant_id,
                          'is_sysadmin': False})
        self.assertTrue(User.objects.filter(username=instance_admin_name_prefix).exists(),
                        "Instance admin was not created")
        user = User.objects.get(username=instance_admin_name_prefix)
        self.assertTrue(user.email == 'insadmin1@mt.de', 'Created instance admin has wrong email')

        self.assertTrue(Tenant.objects.get(id=self.tenant_id).user.filter(id=user.id).exists(),
                        'Created instance admin was not associated to the tenant')

    def test_change_instance_admin_password(self):
        name = '0_instance_admin_to_change'
        user = User.objects.create_user(name, 'adminchange@mt.de', '123456789')
        user.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
        old_password = user.password
        self.client.post(reverse('changeUserPassword', args=[user.id]),
                         {'password': 'abcde', 'password_retry': 'abcde'})
        self.assertNotEqual(old_password, User.objects.get(username=name).password,
                            'Instance admin password was not changed')

        response = self.client.post(reverse('changeUserPassword', args=[user.id]),
                                    {'password': '1234', 'password_retry': 'edcba'})
        self.assertTrue(response.context['invalid'], 'Instance admin password check is incorrect')

        response = self.client.post(reverse('changeUserPassword', args=[42]),
                                    {'password': '1234', 'password_retry': '1234'})
        self.assertRedirects(response,
                             reverse('getUserList'))

    def test_delete_instance_admin(self):
        name = '0_instance_admin_to_delete'
        user = User.objects.create_user(name, 'admindel@mt.de', '123456789')
        user.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
        self.client.post(reverse('deleteUser', args=[user.id]))

        self.assertFalse(User.objects.filter(username=name).exists(),
                         "Instance admin was not correct deleted")
        self.assertRedirects(self.client.post(reverse('deleteUser', args=[42])),
                             reverse('getUserList'))


class SystemAdminTests(TestCase):
    def setUp(self):
        self.client = Client()
        setup_test_environment()  # install a template renderer
        init_permissions(self)
        sysadmin_name = 'testuser'

        user = User.objects.create_user(sysadmin_name, 'test1@mt.de', '123456789')
        user.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
        user.save()

        tenant = Tenant.objects.create(name='tenant1', email='tenant1@mt.de', active=True)
        tenant.save()
        self.tenant_id = tenant.id
        Tenant_Domain.objects.create(name='testserver', tenant=tenant)
        self.client.login(username=sysadmin_name, password='123456789')

    def test_create_system_admin(self):
        self.client.post(reverse('createUser'),
                         {'username': 'sysadmin1', 'email': 'sysadmin1@mt.de', 'password': 'pass',
                          'password_retry': 'pass', 'is_sysadmin': 'True'})
        user = None
        try:
            user = User.objects.get(username='sysadmin1')
        except ObjectDoesNotExist:
            self.assertFalse(True, 'System admin was not created')

        self.assertTrue(user.email == 'sysadmin1@mt.de',
                        'Create system admin failed')

        response = self.client.post(reverse('createUser'),
                                    {'username': 'sysadmin2', 'email': 'a', 'password': 'pass',
                                     'password_retry': 'pass', 'is_sysadmin': 'True'})
        self.assertTrue(response.context['invalid'], 'Email check failed on wrong email "a"')

        response = self.client.post(reverse('createUser'),
                                    {'username': 'sysadmin1', 'email': 'sysadmin1@mt.de', 'password': 'pass',
                                     'password_retry': 'wrongpass', 'is_sysadmin': 'True'})

        self.assertEqual(response.context['invalid'], True, "Password check don't work")

    def test_read_system_admin_list(self):
        compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        user_list_from_db = User.objects.all()
        response = self.client.post(reverse('getUserList'))
        self.assertEqual(compare(user_list_from_db, response.context['adminList']), True,
                         'Reading system admin list failed')

    def test_change_system_admin_password(self):
        user_name = 'sysadmin1'
        self.client.post(reverse('createUser'),
                         {'username': user_name, 'email': 'sysadmin1@mt.de', 'password': 'pass',
                          'password_retry': 'pass', 'is_sysadmin': 'True'})

        user = User.objects.get(username=user_name)
        new_user_pw = '12345'
        old_user_pw_db = user.password
        user_id = user.id
        self.client.post(reverse('changeUserPassword', args=[user_id]),
                         {'password': new_user_pw, 'password_retry': new_user_pw})
        new_user_pw_db = User.objects.get(username=user_name).password
        self.assertNotEqual(old_user_pw_db, new_user_pw_db, 'Change system admin password failed')

        response = self.client.post(reverse('changeUserPassword', args=[user_id]),
                                    {'password': 'abcde', 'password_retry': 'wrong_retry'})

        self.assertEqual(
            (new_user_pw_db == User.objects.get(username=user_name).password) and response.context['invalid'], True,
            'Change system admin password successful with wrong password retry')

        # Test with wrong id
        response = self.client.post(reverse('changeUserPassword', args=[42]),
                                    {'password': 'pass', 'password_retry': 'pass'})
        self.assertRedirects(response, reverse('getUserList'))

    def test_delete_system_admin(self):
        self.client.post(reverse('createUser'),
                         {'username': 'sysadmin1', 'email': 'sysadmin1@mt.de', 'password': 'pass',
                          'password_retry': 'pass', 'is_sysadmin': 'True'})
        user_id = User.objects.get(username='sysadmin1').id
        self.client.post(reverse('deleteUser', args=[user_id]), {'user_id': user_id})
        self.assertFalse(User.objects.filter(id=user_id).exists(), 'Delete system admin failed')

        self.assertRedirects(self.client.post(reverse('deleteUser', args=[42])),
                             reverse('getUserList'))

    def test_change_permission(self):
        admin_name = 'sysadmin1'
        self.client.post(reverse('createUser'),
                         {'username': admin_name, 'email': 'sysadmin1@mt.de', 'password': 'pass',
                          'password_retry': 'pass', 'is_sysadmin': 'False', 'tenant': self.tenant_id})

        user_id = User.objects.get(username='{0}_{1}'.format(self.tenant_id, admin_name)).id

        self.client.post(reverse('changeUserPermission', args=[user_id]),
                         {'hidden_user_id': user_id, 'username': admin_name, 'hidden_was_sysadmin': 'False',
                          'is_sysadmin': 'True'})

        self.assertTrue(User.objects.filter(username=admin_name).exists(),
                        'Changing permission to system admin failed')
        user = User.objects.get(username=admin_name)
        self.assertTrue(user.groups.filter(name=GROUP_SYSTEM_ADMINISTRATOR).exists(),
                        'Changing permission to system admin failed (was not in SystemAdministrator group)')

        # change permission to instance admin
        self.client.post(reverse('changeUserPermission', args=[user_id]),
                         {'hidden_user_id': user_id, 'username': admin_name, 'hidden_was_sysadmin': 'True',
                          'is_sysadmin': 'False', 'tenant': self.tenant_id})
        self.assertTrue(User.objects.filter(username='{0}_{1}'.format(self.tenant_id, admin_name)).exists(),
                        'Changing permission to instance admin failed')
        user = User.objects.get(username='{0}_{1}'.format(self.tenant_id, admin_name))

        self.assertTrue(user.groups.filter(name=GROUP_INSTANCE_ADMINISTRATOR).exists(),
                        'Changing permission to instance admin failed (was not in InstanceAdministrator group)')
        self.assertFalse(user.groups.filter(name=GROUP_SYSTEM_ADMINISTRATOR).exists(),
                         'Changing permission to instance admin failed (was in SystemAdministrator group)')
