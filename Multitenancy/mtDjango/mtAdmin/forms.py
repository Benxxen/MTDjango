from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User

from mtDjango.models import Tenant_Domain, Tenant_DBConn, Tenant


class CreateDirForm(forms.Form):
    name = forms.CharField(label=_('Name'))


# form for login
class LoginForm(forms.Form):
    username = forms.CharField(required=True, label=_('Username'))
    password = forms.CharField(required=True, widget=forms.PasswordInput(), label=_('Password'))


# form to upload a file
class DocumentForm(forms.Form):
    file = forms.FileField(label=_('Select a file'))


# form to change a password
class ChangePasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(), label=_('Password'))
    password_retry = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Repeat password'))

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get('password')
        password_retry = cleaned_data.get('password_retry')
        if password and password != password_retry:
            raise forms.ValidationError(_("The entered passwords didn't match"))
        return cleaned_data


class ChangePermissionForm(forms.Form):
    username = forms.CharField(required=False, label=_('Username'))
    is_sysadmin = forms.BooleanField(required=False, label="Is system administrator")
    tenant = forms.ModelChoiceField(queryset=Tenant.objects.all(), required=False, label=_('Tenant'))
    hidden_user_id = forms.CharField(widget=forms.HiddenInput())
    hidden_was_sysadmin = forms.BooleanField(required=False, widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super(ChangePermissionForm, self).clean()
        username = cleaned_data['username']
        tenant = cleaned_data['tenant']
        is_sysadmin = cleaned_data['is_sysadmin']
        was_sysadmin = cleaned_data['hidden_was_sysadmin']
        if was_sysadmin is is_sysadmin:
            return cleaned_data

        if is_sysadmin and User.objects.filter(username=username).exists():
            raise forms.ValidationError(_('Username already taken'))
        elif not is_sysadmin:
            if not tenant:
                raise forms.ValidationError(_('Tenant is needed'))
            elif Tenant.objects.get(id=tenant.id).user.filter(username='{0}_{1}'.format(tenant.id, username)).exists():
                raise forms.ValidationError(_('Username already taken'))

        return cleaned_data


class ChangeTenantForm(forms.Form):
    name = forms.CharField(label=_('Name'))
    email = forms.EmailField(label=_('Email'))
    active = forms.BooleanField(required=False, label=_('is active'))


class CreateTenantForm(forms.Form):
    name = forms.CharField(required=True, label=_('Name'))
    email = forms.EmailField(required=True, label=_('Email'))
    domain = forms.CharField(required=True, label=_('Domain'))
    active = forms.BooleanField(required=False, label=_('Active'))


class ChangeSystemAdminProfileForm(forms.Form):
    username = forms.CharField(required=False, label=_('Username'))
    email = forms.EmailField(required=False, label=_('Email'))
    password = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Password'))
    password_retry = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Repeat password'))
    hidden_user_id = forms.CharField(required=False, widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super(ChangeSystemAdminProfileForm, self).clean()
        username = cleaned_data['username']
        password = cleaned_data['password']
        password_retry = cleaned_data['password_retry']
        hidden_user_id = cleaned_data['hidden_user_id']

        if password and password != password_retry:
            raise forms.ValidationError(_("The entered passwords didn't match"))

        if username and User.objects.exclude(id=hidden_user_id).filter(username=username).exists():
            raise forms.ValidationError(_('Username already taken'))

        return cleaned_data


class ChangeInstanceAdminProfileForm(forms.Form):
    username = forms.CharField(required=False, label=_('Username'))
    email = forms.EmailField(required=False, label=_('Email'))
    password = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Password'))
    password_retry = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Repeat password'))
    hidden_tenant_id = forms.CharField(required=False, widget=forms.HiddenInput())
    hidden_user_id = forms.CharField(required=False, widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super(ChangeInstanceAdminProfileForm, self).clean()
        username = cleaned_data['username']
        password = cleaned_data['password']
        password_retry = cleaned_data['password_retry']
        hidden_tenant_id = cleaned_data['hidden_tenant_id']
        hidden_user_id = cleaned_data['hidden_user_id']

        if password and password != password_retry:
            raise forms.ValidationError(_("The entered passwords didn't match"))

        if hidden_tenant_id and hidden_user_id:
            if username and Tenant.objects.get(id=hidden_tenant_id).user.exclude(id=hidden_user_id).filter(
                    username='{0}_{1}'.format(hidden_tenant_id, username)).exists():
                raise forms.ValidationError(_('Username already taken'))
        else:
            raise forms.ValidationError(_('Invalid data committed'))

        return cleaned_data


class CreateUserForm(forms.Form):
    username = forms.CharField(required=True, label=_('Username'))
    email = forms.EmailField(required=True, label=_('Email'))
    password = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Password'))
    password_retry = forms.CharField(required=False, widget=forms.PasswordInput(), label=_('Repeat password'))
    is_sysadmin = forms.BooleanField(required=False, label="Is System Administrator")
    tenant = forms.ModelChoiceField(queryset=Tenant.objects.all(), required=False, label=_('Tenant'))

    def clean(self):
        cleaned_data = super(CreateUserForm, self).clean()
        username = cleaned_data['username']
        password = cleaned_data['password']
        password_retry = cleaned_data['password_retry']
        is_sysadmin = cleaned_data['is_sysadmin']
        if password and password != password_retry:
            raise forms.ValidationError(_("The entered passwords didn't match"))

        if is_sysadmin and User.objects.filter(username=username).exists():
            raise forms.ValidationError(_('Username already taken'))
        elif not is_sysadmin:
            tenant = cleaned_data['tenant']
            if not tenant:
                raise forms.ValidationError(_('Tenant is needed'))
            if Tenant.objects.get(id=tenant.id).user.filter(username='{0}_{1}'.format(tenant.id, username)).exists():
                raise forms.ValidationError(_('Username already taken'))

        return cleaned_data


# form to add und edit a domain
class TenantDomainForm(forms.ModelForm):
    class Meta:
        model = Tenant_Domain
        fields = ['name']


# form to config the dbms-configuration
class TenantDBConnForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label=_('Password'))

    class Meta:
        model = Tenant_DBConn
        fields = ['hostname', 'port', 'username', 'dbName', 'engine']
