"""
Wrapper for loading templates from the filesystem.
Using the requested Tenant
"""
import sys
import os
import threading

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils._os import safe_join
from django.utils.importlib import import_module
from django.utils import six
from django.template.loaders.app_directories import Loader

if not six.PY3:
    fs_encoding = sys.getfilesystemencoding() or sys.getdefaultencoding()
app_template_dirs = []
for app in settings.INSTALLED_APPS:
    try:
        mod = import_module(app)
    except ImportError as e:
        raise ImproperlyConfigured('ImportError %s: %s' % (app, e.args[0]))
    template_dir = os.path.join(os.path.dirname(mod.__file__), 'templates')
    if os.path.isdir(template_dir):
        if not six.PY3:
            template_dir = template_dir.decode(fs_encoding)
        app_template_dirs.append(template_dir)

app_template_dirs = tuple(app_template_dirs)


# a templateloader to find the tenant-template
# if the tenant-template doesn't exist then they load a default template
class MTLoader(Loader):
    # make a usabillty
    is_usable = True

    # method to search the template
    def get_template_sources(self, template_name, template_dirs=None):
        if not template_dirs:
            template_dirs = app_template_dirs
        if template_name.find("/") != -1:
            dirs = template_dirs
        else:
            dirs = []
            temp_dir = os.path.dirname(settings.TENANT_DIR)
            tenant = getattr(threading.currentThread(), 'tenant', None)
            dirs.append(temp_dir + "/" + str(tenant) + "/templates")
            dirs.append(temp_dir + "/default/templates")
        for template_dir in dirs:
            try:
                yield safe_join(template_dir, template_name)
            except UnicodeDecodeError:
                raise
            except ValueError:
                pass
