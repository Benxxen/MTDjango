from django import template
from django.contrib.auth.models import Group
from mtDjango.models import GROUP_SYSTEM_ADMINISTRATOR, GROUP_INSTANCE_ADMINISTRATOR

register = template.Library()


@register.filter
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


@register.filter
def is_sadmin(user):
    return user.groups.filter(name=GROUP_SYSTEM_ADMINISTRATOR).exists()


@register.filter
def is_iadmin(user):
    return user.groups.filter(name=GROUP_INSTANCE_ADMINISTRATOR).exists()
