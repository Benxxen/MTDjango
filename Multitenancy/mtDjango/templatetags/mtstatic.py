import os

from django import template
from django.contrib.staticfiles.templatetags.staticfiles import StaticFilesNode
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles import finders

# register the templatetag in the system
from mtDjango.general import get_current_tenant_id

register = template.Library()

# implementation of the templatetag
class MTStaticFilesNode(StaticFilesNode):
    # method to generate the url to the static-file
    def url(self, context):
        path = self.path.resolve(context)

        tenant_id = get_current_tenant_id()
        static_path_relative = str(tenant_id) + os.sep + "static" + os.sep + path
        if finders.find(static_path_relative):
            static_path_absolute = staticfiles_storage.url("") + static_path_relative
        else:
            static_path_absolute = staticfiles_storage.url("") + 'default' + os.sep + 'static' + os.sep + path
        return static_path_absolute


@register.tag('mtstatic')
def do_static(parser, token):
    return MTStaticFilesNode.handle_token(parser, token)
