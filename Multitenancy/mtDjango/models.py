from django.contrib.auth.models import User
from django.db import models

GROUP_SYSTEM_ADMINISTRATOR = 'SystemAdministrator'
GROUP_INSTANCE_ADMINISTRATOR = 'InstanceAdministrator'

class Tenant(models.Model):
    name = models.CharField(max_length=255, unique=True)
    email = models.EmailField()
    active = models.BooleanField(default=True)
    user = models.ManyToManyField(User)
    # db_conn = models.ForeignKey(Tenant_DBConn)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'mt_tenant'


# class to define the domain to the tenant
class Tenant_Domain(models.Model):
    name = models.CharField(max_length=255, primary_key=True)
    tenant = models.ForeignKey(Tenant)

    class Meta:
        db_table = 'mt_domain'


# define the model for a databaseconnection
class Tenant_DBConn(models.Model):
    tenant = models.OneToOneField(Tenant)
    hostname = models.CharField(max_length=255, blank=True)
    port = models.CharField(max_length=6, blank=True)
    username = models.CharField(max_length=255, blank=True)
    password = models.CharField(max_length=255, blank=True)
    dbName = models.CharField(max_length=255, blank=True)
    engine = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'mt_dbconn'

