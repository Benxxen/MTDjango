import threading

# method to get the current tenant
from mtDjango.models import Tenant


def get_current_tenant():
    tenant_id = getattr(threading.currentThread(), 'tenant', None)
    tenant = Tenant.objects.get(id=tenant_id)
    return tenant


# method to get the id of the current tenant
def get_current_tenant_id():
    tenant_id = getattr(threading.currentThread(), 'tenant', None)
    return tenant_id
