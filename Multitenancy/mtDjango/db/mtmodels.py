import threading
import os
import copy

from django.conf import settings
from django.db import models
from django.http import HttpResponseNotFound
from django.db.models import FileField

# Modelmanager for tenantmodel with tenantID
from mtDjango.general import get_current_tenant, get_current_tenant_id
from mtDjango.models import Tenant_DBConn


class TenantMgr(models.Manager):
    def get_queryset(self):
        tenant = get_current_tenant()
        if tenant:
            return super(TenantMgr, self).get_queryset().filter(tenant=tenant.id)
        else:
            return None


# modelmanager for tenantmodels with dedicated dbms
class TenantMgrDBMS(models.Manager):
    def get_queryset(self):
        tenant = get_current_tenant()
        database_id = createDBMS(tenant)
        self._db = database_id
        qs = super(TenantMgrDBMS, self).get_queryset()
        qs.using(database_id)
        return qs


# modelmanager for tenantmodels with tableprefix
class TenantMgrPrefix(models.Manager):
    def get_queryset(self):
        tenant = get_current_tenant()
        mtMeta = copy.copy(self.model._meta)
        if mtMeta.db_table.find("__") > -1:
            arr_db = mtMeta.db_table.split('__', 1)
            mtMeta.db_table = str(tenant.id) + '__' + arr_db[1]
        else:
            mtMeta.db_table = str(tenant.id) + '__' + self.model._meta.db_table

        self.model._meta = mtMeta
        returnVAR = super(TenantMgrPrefix, self).get_queryset()

        return returnVAR


# tenantmodel for dedicated dbms
class MTModelDBMS(models.Model):
    # bind the correct modelmanager
    objects = TenantMgrDBMS()

    # method to save the model
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        tenant = get_current_tenant()
        db_id = createDBMS(tenant)

        returnVAR = super(MTModelDBMS, self).save(force_insert, force_update, db_id, update_fields)
        return returnVAR

    # method to update the model
    def _do_update(self, base_qs, using, pk_val, values, update_fields, forced_update):
        tenant = get_current_tenant()
        db_id = createDBMS(tenant)

        returnVAR = super(MTModelDBMS, self)._do_update(base_qs, db_id, pk_val, values, update_fields, forced_update)
        return returnVAR

    # method to delete the model
    def delete(self, using=None):
        tenant = get_current_tenant()
        db_id = createDBMS(tenant)

        returnVAR = super(MTModelDBMS, self).delete(db_id)
        return returnVAR

    delete.alters_data = True

    # disable the databaseteable create
    class Meta:
        abstract = True


# tenantmodel for tableprefix
class MTModelPrefix(models.Model):
    #bind the correct modelmanager
    objects = TenantMgrPrefix()

    # method to save the model
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        tenant = get_current_tenant()
        mtMeta = copy.copy(self._meta)
        if mtMeta.db_table.find("__") > -1:
            arr_db = mtMeta.db_table.split('__', 1)
            mtMeta.db_table = str(tenant.id) + '__' + arr_db[1]
        else:
            mtMeta.db_table = str(tenant.id) + '__' + self.model._meta.db_table

        self._meta = mtMeta

        returnVAR = super(MTModelPrefix, self).save(force_insert, force_update, using, update_fields)
        return returnVAR

    # method to update the model
    def _do_update(self, base_qs, using, pk_val, values, update_fields, forced_update):

        tenant = get_current_tenant()
        mtMeta = copy.copy(self._meta)
        if mtMeta.db_table.find("__") > -1:
            arr_db = mtMeta.db_table.split('__', 1)
            mtMeta.db_table = str(tenant.id) + '__' + arr_db[1]
        else:
            mtMeta.db_table = str(tenant.id) + '__' + self.model._meta.db_table

        self._meta = mtMeta

        returnVAR = super(MTModelPrefix, self)._do_update(base_qs, using, pk_val, values, update_fields, forced_update)
        return returnVAR

    # method to delete the model
    def delete(self, using=None):
        tenant = get_current_tenant()
        mtMeta = copy.copy(self._meta)
        if mtMeta.db_table.find("__") > -1:
            arr_db = mtMeta.db_table.split('__', 1)
            mtMeta.db_table = str(tenant.id) + '__' + arr_db[1]
        else:
            mtMeta.db_table = str(tenant.id) + '__' + self.model._meta.db_table

        self._meta = mtMeta

        returnVAR = super(MTModelPrefix, self).delete(using)
        return returnVAR

    delete.alters_data = True

    # disable databasetable create
    class Meta:
        abstract = True


# tenant model for models with tenantID
class MTModel(models.Model):
    # declaration for tenantID
    tenant = models.IntegerField(default=-1)

    # bind the correct modelmanager
    objects = TenantMgr()

    # method to save the model
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        tmpTenant = get_current_tenant_id()
        if self.tenant == -1:
            self.tenant = tmpTenant

        returnVAR = super(MTModel, self).save(force_insert, force_update, using, update_fields)
        return returnVAR

    # method to update the model
    # coding is copy from the original django coding
    def _do_update(self, base_qs, using, pk_val, values, update_fields, forced_update):
        # editing the following line and adding a filter for tenant
        filtered = base_qs.filter(pk=pk_val, tenant=self.tenant)

        if not values:
            return update_fields is not None or filtered.exists()
        if self._meta.select_on_save and not forced_update:
            if filtered.exists():
                filtered._update(values)
                return True
            else:
                return False
        return filtered._update(values) > 0

    class Meta:
        abstract = True


# method to create the connection to the dbms temporally
def createDBMS(tenant):
    try:
        dbconn = Tenant_DBConn.objects.get(tenant=tenant)
    except Tenant_DBConn.DoesNotExist:
        return HttpResponseNotFound('<h1>Domain ist gerade inaktiv</h1>')
    database_id = str(tenant.id) + tenant.name
    newDatabase = {}
    newDatabase["id"] = database_id
    newDatabase['ENGINE'] = dbconn.engine
    newDatabase['NAME'] = dbconn.dbName
    newDatabase['USER'] = dbconn.username
    newDatabase['PASSWORD'] = dbconn.password
    newDatabase['HOST'] = dbconn.hostname
    newDatabase['PORT'] = dbconn.port
    settings.DATABASES[database_id] = newDatabase
    return database_id


# class to implement a multi-tenant fileupload
class MTFileField(FileField):
    def generate_filename(self, instance, filename):
        tenant = getattr(threading.currentThread(), 'tenant', None)
        return str(tenant) + os.sep + "media" + os.sep + os.path.join(self.get_directory_name(),
                                                                      self.get_filename(filename))

