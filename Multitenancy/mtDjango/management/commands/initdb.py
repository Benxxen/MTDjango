from optparse import make_option
from sys import stdin

from django.contrib.auth.models import Group

from django.core.management.base import BaseCommand

from mtDjango.management.commands.createtenant import create_tenant
from mtDjango.management.commands.initpermissions import init_permissions
from mtDjango.models import *


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--testing',
                    # action='store_true',
                    dest='testing',
                    default=False,
                    help='Fill DB with dummy values'),
    )

    args = '--testing'
    help = 'Initialize the Database to work with the MT Module'

    def handle(self, *args, **options):
        self.stdout.write('Creating permissions\n')

        init_permissions(self)

        self.stdout.write('\nTenant creation\n')

        if options['testing']:
            create_tenant(self, True)
        else:
            create_tenant(self, False)

        self.stdout.write('\nSystem admin creation\n')

        invalid = True
        if options['testing']:
            username = 'TestSysadmin'
            email = 'tsadmin@mtd.de'
            password = 'fhb123321fhb'
        else:
            username = None
            email = None
            password = None

        while invalid:
            if username is not None and len(username) > 1:
                if email is not None and len(email) > 7 and email.__contains__("@") and email.__contains__("."):
                    if password is not None and len(password) > 4:
                        system_admin = User.objects.create_superuser(username, email, password)
                        system_admin.groups.add(Group.objects.get(name=GROUP_SYSTEM_ADMINISTRATOR))
                        system_admin.groups.add(Group.objects.get(name=GROUP_INSTANCE_ADMINISTRATOR))
                        invalid = False
                    else:
                        if password is not None:
                            self.stdout.write("To short password entered.\n")
                        self.stdout.write("Password:")
                        password = stdin.readline()[:-1]
                else:
                    if email is not None:
                        self.stdout.write("No Valid email address entered.\n")
                    self.stdout.write("Email:")
                    email = stdin.readline()[:-1]
            else:
                if username is not None:
                    self.stdout.write("No name entered.")
                self.stdout.write("Username: ")
                username = stdin.readline()[:-1]
