from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand
from django.db import IntegrityError
from mtDjango.models import GROUP_SYSTEM_ADMINISTRATOR, GROUP_INSTANCE_ADMINISTRATOR


def init_permissions(self):
    try:
        system_admin_group = Group.objects.create(name=GROUP_SYSTEM_ADMINISTRATOR)
        system_admin_group.permissions.add(Permission.objects.get(codename="add_tenant_dbconn"),
                                           Permission.objects.get(codename="change_tenant_dbconn"),
                                           Permission.objects.get(codename="delete_tenant_dbconn"),
                                           Permission.objects.get(codename="add_tenant"),
                                           Permission.objects.get(codename="change_tenant"),
                                           Permission.objects.get(codename="delete_tenant"),
                                           Permission.objects.get(codename="add_tenant_domain"),
                                           Permission.objects.get(codename="change_tenant_domain"),
                                           Permission.objects.get(codename="delete_tenant_domain"),
                                           Permission.objects.get(codename="add_user"),
                                           Permission.objects.get(codename="change_user"),
                                           Permission.objects.get(codename="delete_user"))
    except IntegrityError:
        self.stdout.write('\nSystem administrator permissions already created.\n')

    try:
        tenant_administration_group = Group.objects.create(name=GROUP_INSTANCE_ADMINISTRATOR)
        # tenant_administration_group.permissions.add()
    except IntegrityError:
        self.stdout.write('\nInstance administrator permissions already created.\n')


class Command(BaseCommand):
    args = 'no arguments here'
    help = 'Initialize the instance admin and system admin permissions in Database to work with the MT Module'

    def handle(self, *args, **options):
        init_permissions(self)