import os

from django.conf import settings

from mtDjango.fileLoader import tenantHolder


class PythonFileLoader(object):
    def __init__(self, module_name, tenant):
        self._moduleName = module_name
        self._tenant = tenant
        self._dir_name = ""
        self._file_path = ""

    def set_module_name(self, module_name):
        self._moduleName = module_name

    def set_tenant(self, tenant):
        self._tenant = tenant

    def _check_tenant_dir_exists(self):
        try:
            self._file_path = os.path.dirname(settings.TENANT_DIR)
            for dir_name, dir_names, file_names in os.walk(self._file_path):
                for subdir_name in dir_names:
                    if str(subdir_name).__contains__(self._tenant):
                        return self._check_module_exists(dir_name + os.sep + subdir_name)
            return False
        except:
            return False

    def _check_module_exists(self, dir_name):
        dir_name = dir_name + os.sep + "mtpython"
        for filename in os.walk(dir_name):
            if str(filename).__contains__(self._moduleName):
                self._dir_name = dir_name
                return True
            else:
                return False

    def load_tenant_module(self):
        if self._check_tenant_dir_exists():
            full_file_path = self._file_path + os.sep + self._tenant + os.sep + "mtpython" + os.sep + self._moduleName
            with open(full_file_path) as f:
                contents = f.read()
            module = compile(contents, full_file_path, 'exec')
            ns = {}
            exec(module, ns)
            tenantHolder.mt_instance_list_compiled[self._tenant] = ns
            # else:
            # self.loadDefault()

    # def load_default_module(self):
    #     full_file_path = self._file_path + os.sep + "default" + os.sep + "mtpython" + os.sep + self._moduleName
    #     with open(full_file_path) as f:
    #         contents = f.read()
    #     module = compile(contents, full_file_path, 'exec')
    #     ns = {}
    #     exec(module in ns)
    #     tenantHolder.mt_instance_list_compiled[self._tenant] = ns
    #     print("default")
