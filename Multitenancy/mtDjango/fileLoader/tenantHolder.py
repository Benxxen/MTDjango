__author__ = 'Patrick'

from mtDjango.general import get_current_tenant_id

mt_instance_list = {}
mt_instance_list_compiled = {}


# Gets the tenant specified method from tenant namespace
def get_tenant_method(method_name):
    tenant_id = str(get_current_tenant_id())
    try:
        method = mt_instance_list_compiled[tenant_id][method_name]
        return method
    except:
        return False